"use strict";

class tlDate {
    constructor(_date) {
        if(_date != undefined)
            var date = new Date(_date);
        else
            var date = new Date();

        this.year = date.getFullYear();
        this.month = date.getMonth() + 1;
        this.day = date.getDate();
        this.hour = date.getHours();
        this.minute = date.getMinutes();
    }
}

class tlText {
    constructor(_headline, _text) {
        if(_headline == undefined)
            this.headline = "";
        if(_text == undefined)
            this.text = "";

        this.headline = _headline;
        this.text = _text;
    }
}

class tlPlace {
    constructor(_data) {
        this.name = "";
        this.address = "";
        this.area = "";
        this.latitude = "";
        this.longitude = "";
        this.lat_long_pre = "";
        this.lat_long_post = "";

        if(_data["target-type"] === "place"
           && (_data["type-id"] === "e2c6f697-07dc-38b1-be0b-83d740165532"
               || _data.type === "held at")) {

            if(_data.place.name != undefined
               && _data.place.name != "") {
                this.name = _data.place.name;

                if(_data.place.address != undefined
                   && _data.place.address != "") {
                    this.address = ", " + _data.place.address;
                }

                if(_data.place.area.name != undefined
                   && _data.place.area.name != "") {
                    this.area = ", " + _data.place.area.name;
                }

                if(_data.place.coordinates != undefined) {
                    this.latitude = _data.place.coordinates.latitude;
                    this.longitude = _data.place.coordinates.longitude;
                    this.lat_long_pre = "<a href='https://www.openstreetmap.org/?mlat=" + this.latitude
                        + "&mlon=" + this.longitude
                        + "#map=14/" + this.latitude + "/" + this.longitude+"'>";
                    this.lat_long_post = "</a>";
                }
            }
        }
    }

    get HTML() {
        return this.lat_long_pre
            + this.name
            + this.address
            + this.area
            + this.lat_long_post;
    }
}

class tlPlaces {
    constructor(_event_relations) {
        this.places = [];
        for(var compteur_relation = 0;
            compteur_relation < _event_relations.length;
            compteur_relation++)
        {
            if(_event_relations[compteur_relation]["target-type"] === "place")
            {
                this.places.push(new tlPlace(_event_relations[compteur_relation]));
            }
        }
    }

    get HTML() {
        var html = "";
        if(this.places.length > 0) {
            for(var compteur_place = 0;
                compteur_place < this.places.length;
                compteur_place++)
            {
                html += this.places[compteur_place].HTML + "<br>";
            }
        }
        return html;
    }
}

class tlURL {
    constructor(_data) {
        this.url = "";

        if(_data["target-type"] === "url") {
            this.url = _data.url.resource;
        }
    }

    get HTML() {
        return "<li>" + this.url + "</li>";
    }
}

class tlURLs {
    constructor(_event_relations) {
        this.urls = [];
        for(var compteur_relation = 0;
            compteur_relation < _event_relations.length;
            compteur_relation++)
        {
            if(_event_relations[compteur_relation]["target-type"] === "url")
            {
                this.urls.push(new tlURL(_event_relations[compteur_relation]));
            }
        }
    }

    get HTML() {
        var html = "";
        if(this.urls.length > 0){
            html += "Voir en ligne<br><ul>";
            for(var compteur_url = 0;
                compteur_url < this.urls.length;
                compteur_url++)
            {
                html += this.urls[compteur_url].HTML;
            }
            html += "</ul>";
        }
        return html;
    }
}

class tlSlide {
    constructor(_event_data) {
        this.unique_id = undefined;
        this.start_date = "";
        this.text = "";
        var description = "";
        var cancelled = "";

        var now = new Date();
        if(_event_data["life-span"].begin != undefined) {
            var start = new Date(_event_data["life-span"].begin);
            if(_event_data.time != "") {
                var h = _event_data.time.split(":");
                start.setHours(h[0], h[1], 0);

                this.unique_id = _event_data.id;
                if(_event_data.cancelled) {
                    cancelled = "## ANNULÉ ## - ";
                }

                if(_event_data["life-span"].begin != undefined) {
                    this.start_date = new tlDate(start);
                }
                else {
                    this.start_date = new tlDate(null);
                }

                var places = new tlPlaces(_event_data.relations);
                description += places.HTML;
                var urls = new tlURLs(_event_data.relations);
                description += urls.HTML;

                this.text = new tlText(cancelled + _event_data.name, description);
            }
        }
    }
}

class tlSlides {
    constructor(_artist_data) {
        this._slides = [];
        var valid_event_number = 0;
        if(_artist_data.events.length > 0) {
            for(var compteur_event = 0;
                compteur_event < _artist_data.events.length;
                compteur_event++) {
                var current_slide = new tlSlide(_artist_data.events[compteur_event]);
                if(current_slide.unique_id != undefined) {
                    this._slides.push(current_slide);
                }
            }
        }
    }

    get slides() {
        if(this.empty) {
            return [{"text": {"headline": "Création du trio", "text": ""}, "start_date": { "year": "2019", "month": "08", "day": "01"}}];
        }
        else {
            return this._slides;
        }
    }

    get empty() {
        return this._slides.length == 0;
    }
}

let create_timeline_from_musicbrainz = function(headline, subline) {
    var musicbrainz_client = new XMLHttpRequest();
    musicbrainz_client.open('GET',
                            'https://'+musicbrain_fqdn+'/ws/2/event?artist='+musicbrain_artist_id+'&inc=place-rels+url-rels&fmt=json',
                            false);
    musicbrainz_client.send(null);
    var artist_data = JSON.parse(musicbrainz_client.responseText);
    var timelinejs_data = {"title": {"text": new tlText(headline, subline)}, "events": []};
    var evenements = new tlSlides(artist_data);
    timelinejs_data.events = timelinejs_data.events.concat(evenements.slides);
    return timelinejs_data;
};
